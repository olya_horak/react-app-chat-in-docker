import React from 'react';
import OwnMessage from './OwnMessage';
import Message from './Message';

class MessageList extends React.Component { 

    render() {
        const messages = this.props.messages || [];

        return (
            <div >
                <div className="message-list">
                    {messages.map(message => {
                        if (message.user === "Olya") {
                            return <OwnMessage message={message} onDeleteMessage={this.props.onDeleteMessage}
                                key={`${message.id}`} />;
                        }
                        return <Message message={message}
                            key={`${message.id}`} />;
                    })}
                </div>
                <div className="messages-divider">
                    <hr />
                </div>
            </div>
        );
    }
}
export default MessageList;

/*Список повинен бути розділений по дням
 (.messages-divider) (лінія з вказаним днем, у форматі "Today", "Yesterday",
"Monday, 17 June") */